import time

API_BASE_URL = "http://api.douban.com/v2/movie/"
DOUBAN_MOVIE_SEARCH = API_BASE_URL + 'search?q=%s'
DOUBAN_MOVIE_SUBJECT = API_BASE_URL + 'subject/%s'
DOUBAN_MOVIE_IMDB = API_BASE_URL + 'imdb/%s'
DOUBAN_MOVIE_BASE = 'http://movie.douban.com/subject/%s/'
REQUEST_RETRY_LIMIT = 3

RE_IMDB_ID = Regex('^tt\d+$')
RE_DOUBAN_ID = Regex('\d+$')

####################################################################################################
def GetJSON(url, cache_time=CACHE_1MONTH):
  dbdb_dict = None
  try:
    dbdb_dict = JSON.ObjectFromURL(url, sleep=2.0, headers={'Accept': 'application/json'}, cacheTime=cache_time)
  except:
    Log('Error fetching JSON from Douban Movie Database.')
  return dbdb_dict

def GetImdbId(db_id):
  url = DOUBAN_MOVIE_BASE % db_id
  for t in reversed(range(REQUEST_RETRY_LIMIT)):
    try:
      result = HTML.ElementFromURL(url)
    except:
      Log('Error fetching HTML from Douban Site, will try %s more time(s) before giving up.', str(t))
      time.sleep(5)
      continue
    else:
      break
  if not result:
    return None
  for el in result.xpath('//div[@id="info"]/a[@rel="nofollow"]/text()'):
    m = RE_IMDB_ID.search(el)
    if m:
      return m.group(0)
  return None

def GetDBId(imdb_id):
  # Try to get douban id from imdb_id
  dbdb_dict = GetJSON(url = DOUBAN_MOVIE_IMDB % imdb_id)
  if not dbdb_dict or not dbdb_dict['id']:
    return None
  try:
    db_id = RE_DOUBAN_ID.search(dbdb_dict['id']).group(0)
  except:
    return None
  else:
    return db_id

####################################################################################################
def AppendSearchResult(results, id, name=None, year=-1, score=0, lang=None):

  new_result = dict(id=str(id), name=name, year=int(year), score=score, lang=lang)

  if isinstance(results, list):

    results.append(new_result)

  else:

    results.Append(MetadataSearchResult(**new_result))
################################################################################################


def Start():
  HTTP.CacheTime = CACHE_1WEEK

################################################################################################

class DBDBAgent(Agent.Movies):
  name = 'Douban Movie Database SMTH'
  languages = [Locale.Language.English, Locale.Language.Chinese]

  primary_provider = False
  accepts_from = ['com.plexapp.agents.themoviedb.smth']
  contributes_to = ['com.plexapp.agents.themoviedb.smth']

  def search(self, results, media, lang, manual):
    Log('Try to search %s', media.title)
    # If search is initiated by a different, primary metadata agent.
    # This requires the other agent to use the IMDb id as key.
    if media.primary_metadata is not None and RE_IMDB_ID.search(media.primary_metadata.id):
      results.Append(MetadataSearchResult(id = media.primary_metadata.id, score = 100))
      Log('Already found imdb_id: %s for %s', media.primary_metadata.id, media.title)
      return
    # manual search and name is IMDB id
    if manual and RE_IMDB_ID.search(media.name):
      dbdb_dict = GetJSON(url=DOUBAN_MOVIE_IMDB % media.name)
      if isinstance(dbdb_dict, dict) and 'id' in dbdb_dict:
        results.Append(MetadataSearchResult(
          id = media.name,
          name = dbdb_dict['title'],
          year = dbdb_dict['year'][0],
          lang = lang))
        return

    # automatic search
    dbdb_dict = None
    retries = 0
    while dbdb_dict is None and retries < 3:
      dbdb_dict = GetJSON(url=DOUBAN_MOVIE_SEARCH % String.Quote(media.title))
    Log('dbdb_dict: %s', str(dbdb_dict))
    if isinstance(dbdb_dict, dict) and 'subjects' in dbdb_dict:
      for i, movie in enumerate(sorted(dbdb_dict['subjects'], key=lambda k: k['collect_count'], reverse=True)):
        # if it's episode, then continue
        if movie['subtype'] != 'movie':
          continue
        score = 90
        score = score - min(abs(String.LevenshteinDistance(movie['original_title'].lower(), media.title.lower())),
                            abs(String.LevenshteinDistance(movie['title'].lower(), media.title.lower())))
        score = score - (5 * i)
        Log('score: %s', str(score))
        release_year = None
        if 'year' in movie and movie['year']:
          release_year = int(movie['year'])
        else:
          release_year = -1

        if media.year and int(media.year) > 1900 and release_year:
          year_diff = abs(int(media.year) - release_year)

          if year_diff <= 1:
            score = score + 10
          else:
            score = score - (5 * year_diff)
        Log('score: %s', str(score))
        if score <= 0:
          continue
        else:
          imdb_id = GetImdbId(movie['id'])
          if imdb_id:
            Log('Add result (%s, %s, %s, %s, %s) for %s', imdb_id, movie['title'], str(release_year), str(score), lang, media.title)
            AppendSearchResult(results=results,
                               id=imdb_id,
                               name=movie['title'],
                               year=release_year,
                               score=score,
                               lang=lang)


  def update(self, metadata, media, lang):
    imdb_id = None
    if RE_IMDB_ID.search(media.id):
      imdb_id = media.id
    else:
      if RE_IMDB_ID.search(metadata.id):
        imdb_id = metadata.id
    if not imdb_id:
      Log('No imdb id for %s %s', media.id, media.title)
      return
    else:
      Log('Try to update %s %s %s', media.id, media.title, imdb_id)

    db_id = GetDBId(imdb_id)
    if not db_id:
      Log('Cannot find douban id for %s %s', media.id, media.title)
      return
    for t in reversed(range(REQUEST_RETRY_LIMIT)):
      try:
        dbdb_dict = GetJSON( url = DOUBAN_MOVIE_SUBJECT % db_id)
      except:
        Log('Cannot get info for %s %s %s, will try %s more time(s) before giving up.', media.id, media.title, db_id, str(t))
        time.sleep(30)
        continue
      else:
        break
    if not dbdb_dict:
      Log('Give up for %s %s', media.id, media.title)
      return
    # Rating
    votes = dbdb_dict['ratings_count']
    rating =  dbdb_dict['rating']['average']
    if votes > 3:
      metadata.rating = float(rating)

    # Title
    metadata.title = dbdb_dict['title']

    if metadata.title != dbdb_dict['original_title']:
      metadata.original_title = dbdb_dict['original_title']

    # Summary
    if dbdb_dict['summary']:
      metadata.summary = dbdb_dict['summary']

    # Genres
    metadata.genres.clear()
    for genre in dbdb_dict['genres']:
      metadata.genres.add(genre.strip())

    # Tagline
    #metadata.tagline = ' '.join([ i['name'] for i in dbdb_dict['tags'] ])

    # Directors
    metadata.directors.clear()
    for member in dbdb_dict['directors']:
      metadata.directors.add(member['name'])

    # Writers
    #metadata.writers.clear()
    #for member in dbdb_dict['attrs']['writer']:
    #  metadata.writers.add(member)

    # Casts
    metadata.roles.clear()
    for member in dbdb_dict['casts']:
      role = metadata.roles.new()
      role.actor = member['name']

    if len(metadata.posters.keys()) == 0:
      poster_url = dbdb_dict['images']['large']
      thumb_url = dbdb_dict['images']['small']
      metadata.posters[poster_url] = Proxy.Preview(HTTP.Request(thumb_url), sort_order=1)
